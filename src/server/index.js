const fs = require('fs');
const ipl = require("./ipl.js")
const extra = require("./extra.js")
var csvToJson = require('convert-csv-to-json');
var matchArr = csvToJson.fieldDelimiter(',').getJsonFromCsv("/home/mausam/Desktop/ipl/src/data/matches.csv");
var deliveryArr = csvToJson.fieldDelimiter(',').getJsonFromCsv("/home/mausam/Desktop/ipl/src/data/deliveries.csv");
function generateOutputFile(name,obj) {
    var content = JSON.stringify(obj);
    fs.writeFileSync(`/home/mausam/Desktop/ipl/src/output/${name}`,content);
}

console.log(ipl.matchesPlayedPerYear(matchArr));
console.log(ipl.matchesWonPerTeamPerYear(matchArr));
console.log(ipl.extraRunsConcededByBowlingTeams(deliveryArr,matchArr));
console.log(ipl.topTenEconomicBowlers(deliveryArr,matchArr));

console.log(extra.tossWonMatchWon(matchArr));
console.log(extra.playerOfTheMatchPerSeason(matchArr));
console.log(extra.playerDismissedByAnother(deliveryArr));
console.log(extra.bestEconomyInSuperover(deliveryArr));

generateOutputFile('matchesPlayed.json',ipl.matchesPlayedPerYear(matchArr));
generateOutputFile('matchesWon.json',ipl.matchesWonPerTeamPerYear(matchArr));
generateOutputFile('extraRuns.json',ipl.extraRunsConcededByBowlingTeams(deliveryArr,matchArr));
generateOutputFile('topTenBowlers.json',ipl.topTenEconomicBowlers(deliveryArr,matchArr));


generateOutputFile('tossMatchWon.json',extra.tossWonMatchWon(matchArr));
generateOutputFile('maxPlayerOfTheMatchWon.json',extra.playerOfTheMatchPerSeason(matchArr));
generateOutputFile('playersDismissed.json',extra.playerDismissedByAnother(deliveryArr));
generateOutputFile('superOverEconomy.json',extra.bestEconomyInSuperover(deliveryArr)); 
