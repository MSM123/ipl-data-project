module.exports = {

//1.Find the number of times each team won the toss and also won the match

tossWonMatchWon : function(matchArr) {
    const tossAndMatchWinner = matchArr.filter((currMatch) => {
      return currMatch.toss_winner === currMatch.winner;
    });
    const numberOfTimes = tossAndMatchWinner.reduce((teamList,currMatch) => {
  if(!teamList[currMatch.toss_winner]) {
    teamList[currMatch.toss_winner] = 1;
  }
  else {
    teamList[currMatch.toss_winner]++;
  }
  return teamList;
    },{});
    return numberOfTimes;
     },

//2. Find player per season who has won the highest number of Player of the Match awards

playerOfTheMatchPerSeason:function(matchArr) {
    const allPlayerOfTheMatch = matchArr.reduce((obj,match)=> {
  if (!obj[match.season]) {
    obj[match.season] = {};
  }
  if(!obj[match.season][match.player_of_match]) {
    obj[match.season][match.player_of_match] = 1;
  }
  else {
    obj[match.season][match.player_of_match]++;
  }
  return obj
    },{});
   const allPlayerOfTheMatchArr = Object.entries(allPlayerOfTheMatch);
   
  },

//3. Find the highest number of times one player has been dismissed by another player

playerDismissedByAnother : function(deliveryArr) {
const wicketTaken = deliveryArr.filter(wicket => {
    if((wicket['player_dismissed'] !== '') && (wicket['dismissal_kind'] !== 'run out')) {
        return wicket;
    }
});
const wicketAll = wicketTaken.reduce((playerDismised,currWicket) => {
    if(!playerDismised[currWicket['bowler']]) {
        playerDismised[currWicket['bowler']] = {};
    }
    if(!playerDismised[currWicket['bowler']][currWicket['player_dismissed']]) {
        playerDismised[currWicket['bowler']][currWicket['player_dismissed']] = 1;
    }
    else {
        playerDismised[currWicket['bowler']][currWicket['player_dismissed']]++;
    }
    return playerDismised;
},{});
const wicketAllArr = Object.entries(wicketAll);
return wicketAllArr;
},

//4. Find the bowler with the best economy in super overs

bestEconomyInSuperover : function(deliveryArr) {
    const superOverMatch = deliveryArr.filter(superOverDelivery => {
        if (superOverDelivery['is_super_over'] !== '0') {
return superOverDelivery;
        }
    });
    const runAndBall = superOverMatch.reduce((currBowler,currOver) => {
if(!currBowler[currOver['bowler']]) {
    currBowler[currOver['bowler']] = {ball : 0 , run : 0};
}
if((currOver['noball_runs'] === '0') && (currOver['wide_runs'] === '0')) {
    currBowler[currOver['bowler']]['ball']++;
}
currBowler[currOver['bowler']]['run'] += parseInt(currOver['total_runs']);
return currBowler;
    },{});
    const runBallArr = Object.entries(runAndBall);
  const economyRates = runBallArr.reduce((economy,curr) => {
    if(!economy[curr[0]]) {
      economy[curr[0]] = (curr[1]['run']*6)/curr[1]['ball'];
    }
    return economy;
  },{});
   economyRatesArr = Object.entries(economyRates);
   return (economyRatesArr.sort((a,b)=>a[1]-b[1]))[0];
} 

 }
