module.exports={

//1.Number of matches played per year for all the years in IPL

  matchesPlayedPerYear:function(matchArr) {
    const matchPlayed = matchArr.reduce((obj,matches) => {
        if (!obj[matches.season]) {
          obj[matches.season]=1;
        }
         else {
          obj[matches.season]++;
        }
        return obj;
      }, {});
  return matchPlayed;
},

//2.Number of matches won of per team per year in IPL

matchesWonPerTeamPerYear: function(matchArr) {
  const matchesWonPerTeam = matchArr.reduce((winnerTeam,currMatch) => {
    if(!winnerTeam[currMatch['winner']]) {
        winnerTeam[currMatch['winner']] = {};
    }
    if(!winnerTeam[currMatch['winner']][currMatch['season']]){
      winnerTeam[currMatch['winner']][currMatch['season']]=1;
    } 
    else {
      winnerTeam[currMatch['winner']][currMatch['season']]++;
    }
    return winnerTeam;
  },{});
return matchesWonPerTeam;
},

//function written for the target year
 matchIdYear : function(matchesArr, year) {
  return matchesArr.filter(match => match.season === year).map(match => match['id']);
},


//3.Extra runs conceded per team in 2016

extraRunsConcededByBowlingTeams:function(deliveryArr,matchArr) {
  /*
  const matchesPlayedId = matchArr.filter((matchArray) => {
    return (matchArray.season === '2016');
}).map((matchIdArray) => {
    return matchIdArray.id;
}); */
const matchesPlayedId = matchIdYear(matchArr,'2016');
const matchId = deliveryArr.filter((deliveryArray) => {
    return matchesPlayedId.indexOf(deliveryArray.match_id) > -1;
});
const extraRunsConceded = matchId.reduce((extraRunsTeam,currMatch) => {
if(!extraRunsTeam[currMatch['bowling_team']]) {
  extraRunsTeam[currMatch['bowling_team']] = parseInt(currMatch['extra_runs']);
}
else {
  extraRunsTeam[currMatch['bowling_team']] = parseInt(extraRunsTeam[currMatch['bowling_team']]) + parseInt(currMatch['extra_runs']);
}
return extraRunsTeam;
},{});
return extraRunsConceded;
},

//4.Top 10 economical bowlers in 2015

topTenEconomicBowlers:function(deliveryArr,matchArr) {
  /*
  const matchesPlayedInYear = matchArr.filter((matchArray) => {
    return (matchArray.season === '2015');
}).map((matchPlayedId) => {
    return matchPlayedId.id;
}); */
const matchesPlayedInYear = matchIdYear(matchArr,'2015');
const matchIdInDeliveries = deliveryArr.filter((deliveryMatch) => {
    return matchesPlayedInYear.indexOf(deliveryMatch.match_id) > -1
});
const bowlers = matchIdInDeliveries.reduce((bowlersList,matchIdArr) => {
    if(bowlersList.indexOf(matchIdArr.bowler) < 0) {
        bowlersList.push(matchIdArr.bowler);
    }
    return bowlersList;
},[]);
const economyRates = bowlers.reduce((economy,bowlerName) => {
    let deliveriesByBowler = matchIdInDeliveries.filter(delivery => delivery.bowler === bowlerName);
if(deliveriesByBowler.length > 29) {
    let runsGivenByBowler = deliveriesByBowler.reduce((runs,delivery) => runs + parseInt(delivery.total_runs,10),0);

let economyRate = Math.floor(runsGivenByBowler*6/ (deliveriesByBowler.length));
economy.push([bowlerName,economyRate]);
}
return economy;
},[]);
economyRates.sort((a,b) => a[1] - b[1]);
let topTenEconomicalBowler = economyRates.slice(0,10);
return topTenEconomicalBowler;
}

}