function iplHighcharts(){
    fetch('/src/output/matchesPlayed.json')
    .then(response => response.json())
    .then(data => {
        Highcharts.chart('matchesPlayed', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Number of Matches played in Each season'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of matches'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Number of matches: <b>{point.y:.1f}</b>'
            },
            series: [{
                name: 'Population',
                data: Object.entries(data),
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
    fetch('/src/output/extraRuns.json')
    .then(response => response.json())
    .then(data => {
        Highcharts.chart('extraRunsConceded', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Extra Runs conceded per team in 2016'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Extra runs conceded'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Extra runs conceded in 2016: <b>{point.y:.1f}</b>'
            },
            series: [{
                name: 'Population',
                data: Object.entries(data),
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });

    fetch('/src/output/topTenBowlers.json')
    .then(response => response.json())
    .then(data => {
        Highcharts.chart('topEconomicalBowler', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top 10 economical bowlers in 2015'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Economy of bowler'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Economy rate: <b>{point.y:.1f}</b>'
            },
            series: [{
                name: 'Population',
                data: data,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
    fetch('/src/output/matchesWon.json')
    .then(response => response.json())
    .then(data3 =>
      Highcharts.chart('matchesWonPerSeason', {
        chart: {
          type: 'column'
        },
        title: {
          text: 'Matches Won Per Team Per Year'
        },
        subtitle: {
          text: 'Source: kaggle.com'
        },
        xAxis: {
          title: {
            text: 'Teams'
          },
          categories: [
            ...new Set(
              Object.values(data3)
                .map(values => Object.keys(values))
                .reduce((acc, curr) => acc.concat(curr), [])
                .sort()
            )
          ]
        },
        yAxis: {
          // min: -1,
          title: {
            text: 'Years'
          },
          stackLabels: {
            enabled: true,
            style: {
              fontWeight: 'bold',
              color:
                // theme
                (Highcharts.defaultOptions.title.style &&
                  Highcharts.defaultOptions.title.style.color) ||
                'gray'
            }
          }
        },
        legend: {
          align: 'right',
          x: -30,
          verticalAlign: 'top',
          y: 25,
          floating: true,
          backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
          borderColor: '#CCC',
          borderWidth: 1,
          shadow: false
        },
        tooltip: {
          headerFormat: '<b>{point.x}</b><br/>',
          pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
          column: {
            stacking: 'normal',
            dataLabels: {
              enabled: false
            }
          }
        },

        series: app(data3).reduce((team, winningData) => {
          team.push({
            name: winningData[0],
            data: winningData[1]
          });
          return team;
        }, [])
      })
    )
    .catch(error => console.log('error ' + error));
}
    